package main

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/devops-experimental/test-grpc/proto"
	"google.golang.org/grpc" // Update the import path to your actual proto package
)

func main() {
	// Set up a connection to the gRPC server
	conn, err := grpc.Dial("103.150.190.54:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to connect: %v", err)
	}
	defer conn.Close()

	// Create client for ProductService
	productClient := proto.NewProductServiceClient(conn)

	// Test GetProductById RPC
	productID := "123"
	getProductRequest := &proto.GetProductByIdRequest{
		ProductId: productID,
	}

	product, err := productClient.GetProductById(context.Background(), getProductRequest)
	if err != nil {
		log.Fatalf("Failed to call GetProductById: %v", err)
	}

	fmt.Printf("Product ID: %s\n", product.GetId())
	fmt.Printf("Product Name: %s\n", product.GetName())

	// Set up a connection to the gRPC server
	conn, err = grpc.Dial("103.150.190.54:50052", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to connect: %v", err)
	}
	defer conn.Close()

	// Create client for OrderService
	orderClient := proto.NewOrderServiceClient(conn)

	// Test CreateOrder RPC
	orderID := "456"
	productID = "123"
	createOrderRequest := &proto.CreateOrderRequest{
		OrderId:   orderID,
		ProductId: productID,
	}

	order, err := orderClient.CreateOrder(context.Background(), createOrderRequest)
	if err != nil {
		log.Fatalf("Failed to call CreateOrder: %v", err)
	}

	fmt.Println("")
	fmt.Printf("Order ID: %s\n", order.GetId())
	fmt.Printf("Order Name: %s\n", order.GetName())
	fmt.Printf("Product ID in Order: %s\n", order.GetProduct().GetId())
	fmt.Printf("Product Name in Order: %s\n", order.GetProduct().GetName())

	// Set up a connection to the gRPC server
	conn, err = grpc.Dial("103.150.190.54:50053", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to connect: %v", err)
	}
	defer conn.Close()

	// Create client for DeliveryService
	deliveryClient := proto.NewDeliveryServiceClient(conn)

	// Test DeliverOrder RPC
	orderID = "123"
	deliverOrderRequest := &proto.DeliverOrderRequest{
		OrderId: orderID,
	}

	deliveryResponse, err := deliveryClient.DeliverOrder(context.Background(), deliverOrderRequest)
	if err != nil {
		log.Fatalf("Failed to call DeliverOrder: %v", err)
	}

	fmt.Printf("Delivery Response: %s\n", deliveryResponse.GetMessage())
}
